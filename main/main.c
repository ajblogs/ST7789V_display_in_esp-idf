/* psu.c
   Author: Anuj.j (Anujfalcon8@gmail.com)


   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/*
 This code displays some fancy graphics on the 320x240 LCD on an ESP-WROVER_KIT board.
 This example demonstrates the use of both spi_device_transmit as well as
 spi_device_queue_trans/spi_device_get_trans_result and pre-transmit callbacks.

 Some info about the ILI9341/ST7789V: It has an C/D line, which is connected to a GPIO here. It expects this
 line to be low for a command and high for data. We use a pre-transmit callback here to control that
 line: every transaction has as the user-definable argument the needed state of the D/C line and just
 before the transaction is sent, the callback will set this line to the correct state.
*/

// #define PIN_NUM_MISO 25
#define PIN_NUM_MOSI 19
#define PIN_NUM_CLK  18
#define PIN_NUM_CS   5

#define PIN_NUM_DC   16
#define PIN_NUM_RST  23
#define PIN_NUM_BCKL 4

//To speed up transfers, every SPI transfer sends a bunch of lines. This define specifies how many. More means more memory use,
//but less overhead for setting up / finishing transfers. Make sure 240 is dividable by this.

#include "lcd.h"

void app_main()
{
    printf("Begin SPI \n");
    lcd.spi = spi_setup();
    lcd.background = 0xFF3300;

    lcd_fill(lcd.background ,&lcd);
    float u = 1.34589;

    char v[11]={};
    char a[11]={};
    sprintf(v,"%06.3f V",u);
    sprintf(a,"%06.3f A",u);
    printf(v);
    printf("\n");

    lcd_square(0x00ff00,10,0,97,33,&lcd);
    lcd_square(0x00ff00,133,0,97,33,&lcd);

    font32.font_back_rgb24 = 0xff3300;
    font32.font_rgb24 = 0x000000;
    int w = lcd_text(10,3,v,&font32);
    printf("%d \n",w);
    font32.font_rgb24 = 0xffffff;
    lcd_text(w+26,3,a,&font32);

    int val[20]={20,-20,0,9,23,0,4,5,6,7,-1,-2,-3,-25,25,0,25,3,2,1};
    
    for(int j=0;j<12;j++){    
        for(int i=0;i<20;i++){
            graph_plot(val[i],Volt);
        }
        for(int i=0;i<20;i++){
            graph_plot(val[i],Amp);
        }
    }

}
