#include <Fonts/Font72x53rle.c>

#define nr_chrs_f72x53 96
#define chr_hgt_f72x53 75
#define baseline_f72x53 73
#define data_size_f72x53 8
#define firstchr_f72x53 32
#define max_width_f72x53 53

extern const unsigned char widtbl_f72x53[96];
extern const unsigned char* const chrtbl_f72x53[96];
