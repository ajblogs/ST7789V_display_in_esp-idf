/* lcd.h
   Author: Anuj.j (Anujfalcon8@gmail.com)

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "Fonts/Font16.h"
#include "Fonts/Font32rle.h"
// #include "Fonts/Font64rle.h"
// #include "Fonts/Font72rle.h"

#define PIX_WIDTH 240
#define PIX_WIDTH_START 40
#define PIX_WIDTH_END 279
#define PIX_HEIGHT 135
#define PIX_HEIGHT_START 53
#define PIX_HEIGHT_END 187

#define PARALLEL_LINES 135

typedef int color;

/*
 The LCD needs a bunch of command/argument values to be initialized. They are stored in this struct.
*/
typedef struct {
    uint8_t cmd;
    uint8_t data[16];
    uint8_t databytes; //No of data in data; bit 7 = delay after set; 0xFF = end of cmds.
} lcd_init_cmd_t;

typedef struct lcd_struct lcd_struct;
struct lcd_struct{
    //Solid background color, which used as reference when color = -1
    color background;
    spi_device_handle_t spi;
};

typedef struct font_struct font_struct;
struct font_struct{
    int height;
    int max_width;
    int firstchr_idx;
    const unsigned char  *widtbl;
    const unsigned char * const*  chrtbl;
    void (*decoder)(font_struct *, char, uint16_t*);
    color font_rgb24;
    color font_back_rgb24;
    // uint16_t *disp_data;
    spi_device_handle_t spi;
};


typedef struct graph_struct graph_struct;
struct graph_struct{
    // equal ranges for +/- if even,
    // +ve range is less by one than -ve ranges for +/- if odd,
    //Also the graph_sheet_height value
    int Y_range;
    // int graph_sheet_height = Y_range;
    int sheet_width;
    int plot_width;
    color plot_V_color;
    color back_V_color;
    const int V_x_offset;
    const int V_y_offset;
    color plot_A_color;
    color back_A_color;
    const int A_x_offset;
    const int A_y_offset;
};

//Place data into DRAM. Constant data gets placed into DROM by default, which is not accessible by DMA.
DRAM_ATTR static const lcd_init_cmd_t st_init_cmds[]={
    /* Memory Data Access Control, MX=MV=1, MY=ML=MH=0, RGB=0 */
    {0x36, {(1<<5)|(1<<6)}, 1},
    /* Interface Pixel Format, 16bits/pixel for RGB/MCU interface */
    {0x3A, {0x55}, 1},
    /* Porch Setting */
    {0xB2, {0x0c, 0x0c, 0x00, 0x33, 0x33}, 5},
    /* Gate Control, Vgh=13.65V, Vgl=-10.43V */
    {0xB7, {0x45}, 1},
    /* VCOM Setting, VCOM=1.175V */
    {0xBB, {0x2B}, 1},
    /* LCM Control, XOR: BGR, MX, MH */
    {0xC0, {0x2C}, 1},
    /* VDV and VRH Command Enable, enable=1 */
    {0xC2, {0x01, 0xff}, 2},
    /* VRH Set, Vap=4.4+... */
    {0xC3, {0x11}, 1},
    /* VDV Set, VDV=0 */
    {0xC4, {0x20}, 1},
    /* Frame Rate Control, 60Hz, inversion=0 */
    {0xC6, {0x0f}, 1},
    /* Power Control 1, AVDD=6.8V, AVCL=-4.8V, VDDS=2.3V */
    {0xD0, {0xA4, 0xA1}, 1},
    /* Positive Voltage Gamma Control */
    {0xE0, {0xD0, 0x00, 0x05, 0x0E, 0x15, 0x0D, 0x37, 0x43, 0x47, 0x09, 0x15, 0x12, 0x16, 0x19}, 14},
    /* Negative Voltage Gamma Control */
    {0xE1, {0xD0, 0x00, 0x05, 0x0D, 0x0C, 0x06, 0x2D, 0x44, 0x40, 0x0E, 0x1C, 0x18, 0x16, 0x19}, 14},
    /* Sleep Out */
    {0x11, {0}, 0x80},
    /* Display On */
    {0x29, {0}, 0x80},
    {0, {0}, 0xff}
};

void lcd_init(spi_device_handle_t spi);
void lcd_spi_pre_transfer_callback(spi_transaction_t *t);
spi_device_handle_t spi_setup();

void lcd_cmd(spi_device_handle_t spi, const uint8_t cmd);
void lcd_data(spi_device_handle_t spi, const uint8_t *data, int len);

void send_lines(int ypos, spi_device_handle_t spi, uint16_t *linedata);
void send_square(int xpos, int ypos, int xWidth, int yHeight, spi_device_handle_t spi, uint16_t *sqdata);
void send_finish(spi_device_handle_t spi);

color rgb24_16(color rgb);
void font16_decoder(font_struct *f, char c, uint16_t* display_data);
void font_rle_decoder(font_struct *f, char c, uint16_t* display_data);

void lcd_fill(color rgb24, lcd_struct*);
void lcd_square(color rgb24, int xpos, int ypos, int xWidth,int yHeight, lcd_struct *);
void lcd_char(int xpos,int ypos, char c, font_struct *font);
int lcd_text(int xpos,int ypos, char *c,font_struct *font);


lcd_struct lcd = {0x0,NULL};
font_struct font16 = {chr_hgt_f16,max_width_f16,firstchr_f16,widtbl_f16,chrtbl_f16,font16_decoder,0x0,-1,NULL};
font_struct font32 = {chr_hgt_f32,max_width_f32,firstchr_f32,widtbl_f32,chrtbl_f32,font_rle_decoder,0x0,-1,NULL};
// font_struct font64 = {chr_hgt_f64,max_width_f64,firstchr_f64,widtbl_f64,chrtbl_f64,font_rle_decoder,0x0,-1,NULL};
// font_struct font72 = {chr_hgt_f72,max_width_f72,firstchr_f72,widtbl_f72,chrtbl_f72,font_rle_decoder,0x0,-1,NULL};


bool Volt = true;
bool Amp = false;
//graph general setting,graph V setting,graph A setting 
graph_struct graph_settings = {50,PIX_WIDTH,1,0x0,0xFFFFFF,0,33,0xFF0000,0xFFFFFF,0,85};
// true for Volt, false for Amp
void graph_plot(int value, bool Volt);