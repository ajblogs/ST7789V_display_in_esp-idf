# ST7789V-Display_in_ESP-IDF

Utilize ST7789V controlled displays with official development framework for ESP32 (ESP-IDF) i.e. programming TTGO-T-Display modules with ESP-IDF in Ubuntu. The Project is a direct drop-in example to be used with ESP-IDF framework.

ESP-IDF: https://github.com/espressif/esp-idf<br/>
ESP-IDF installation steps: https://docs.espressif.com/projects/esp-idf/en/stable/get-started/index.html#step-2-get-esp-idf<br/>
ESP-IDF API Guide : https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference

TTGO-T-Display: https://github.com/Xinyuan-LilyGO/TTGO-T-Display

The project stemmed out of the real-time reliable controlling needs for utilizing  
the module with FreeRTOS to its full potential using the officially supported framework.  
This brings the project closer to the source of APIs, giving us a great deal of  
power for understanding whats happening under the hood with less brainwork (security  
reasons!), to utilize the complete customization and optimization opportunities as  
provided by the manufacturers of the ESP32 SoC themselves while making it less  
susceptible to the changes and limitations of the ported versions used in external  
IDEs. 

The downside to this approach is the could be lack of direct support in terms of  
libraries focussed on different applications as available on platforms like Arduino  
and lack of OOP ( The framework uses C **not *C++***).

***Closer the source, lesser the dependency links and susceptibility is, thus  
improving the reliability of the Application. Such a choice is need dependent!***

Tools: Ubuntu 19.10, VScode, ESP32 TTGO-T-Display, Type C cable <br/>

<img src="docs/working.jpg" width="40%">

Prerequisite: ESP-IDF should be installed. Follow the steps from official [link](https://docs.espressif.com/projects/esp-idf/en/stable/get-started/index.html#step-2-get-esp-idf)

- Connect the module (ESP32) to computer's USB port
- `git clone https://gitlab.com/ajblogs/ttgo-t-display_in_esp-idf` 
- `cd ST7789V_display_in_esp-idf/`
- `idf.py -p /dev/ttyUSB0 monitor flash`<br/> 
*Note 1*: Please replace `/dev/ttyUSB0` with your com port value to which ESP32 is attached<br/>
*Note 2*: Steps are almost similar for other OSs. Refer the [link](https://docs.espressif.com/projects/esp-idf/en/stable/get-started/index.html#step-2-get-esp-idf)

## Fonts

Fonts: https://github.com/Bodmer/TFT_eSPI/tree/master/Fonts  
*Note*: Currently RLE and RAW(Max 16bits per row) format of fonts are supported.


- Copy the font file \*.c/\*.h to PSU/Fonts
- Add `#define max_width <max val from widtbl>` which is utilized for dynamic heap memory allocation to  *.h file.
- If the font is other than RLE or RAW encoded, create your own font decoder else continue
- `#include` the *.h file in lcd.h
- Create a `font_struct` for the added font in lcd.h (The existing definitions and typename is expected to guide oneself)

Upon failure, it can be concluded that either one messed it up **or** the font file doesn't have the proper data structure *(Refer existing fonts in PSU/Fonts)*
